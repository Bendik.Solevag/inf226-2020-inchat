package inf226.inchat;

public final class UserName {
    private final String userName;

    public UserName(String userName){
        this.userName = userName;
    }

    public String getUserName(){
        return userName;
    }


}
