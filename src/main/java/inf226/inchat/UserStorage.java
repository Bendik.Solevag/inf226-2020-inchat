package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;
import inf226.util.*;




public final class UserStorage
    implements Storage<User,SQLException> {
    
    final Connection connection;
    
    public UserStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS User (id TEXT PRIMARY KEY, version TEXT, name TEXT, joined TEXT)");
    }
    
    @Override
    public Stored<User> save(User user)
      throws SQLException {
        final Stored<User> stored = new Stored<User>(user);
        PreparedStatement statement = connection.prepareStatement("INSERT INTO User VALUES (?, ?, ?, ?)");
        statement.setString(1, stored.identity.toString());
        statement.setString(2, stored.version.toString());
        statement.setString(3, user.name.getUserName());
        statement.setString(4, user.joined.toString());
        statement.execute();
        return stored;
    }
    
    @Override
    public synchronized Stored<User> update(Stored<User> user,
                                            User new_user)
        throws UpdatedException,
            DeletedException,
            SQLException {
        final Stored<User> current = get(user.identity);
        final Stored<User> updated = current.newVersion(new_user);
        if(current.version.equals(user.version)) {
            PreparedStatement statement = connection.prepareStatement("UPDATE User SET (version, name, joined) = (?, ?, ?) WHERE id = ?");
            statement.setString(1, updated.version.toString());
            statement.setString(2, new_user.name.getUserName());
            statement.setString(3, new_user.joined.toString());
            statement.setString(4, updated.identity.toString());
            statement.execute();
        } else {
            throw new UpdatedException(current);
        }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<User> user)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<User> current = get(user.identity);
        if(current.version.equals(user.version)) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM User WHERE id = ?");
            statement.setString(1, user.identity.toString());
            statement.execute();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<User> get(UUID id)
      throws DeletedException,
             SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT version, name, joined FROM User WHERE id = ?");
        statement.setString(1, id.toString());
        final ResultSet rs = statement.executeQuery();

        if(rs.next()) {
            final UUID version = 
                UUID.fromString(rs.getString("version"));
            final String name = rs.getString("name");
            final Instant joined = Instant.parse(rs.getString("joined"));
            return (new Stored<User>
                        (new User(name,joined),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up a user by their username;
     **/
    public Maybe<Stored<User>> lookup(String name) {
        try{
            PreparedStatement statement = connection.prepareStatement("SELECT id FROM User WHERE name = ?");
            statement.setString(1, name);
            final ResultSet rs = statement.executeQuery();
            if(rs.next())
                return Maybe.just(
                    get(UUID.fromString(rs.getString("id"))));
        } catch (Exception e) {
        
        }
        return Maybe.nothing();
    }
}


