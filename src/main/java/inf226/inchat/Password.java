package inf226.inchat;
import com.lambdaworks.crypto.SCryptUtil;

public final class Password {

    private final String encodedPass;

    public Password(String password){
        this.encodedPass = password;
    }

    public String getEncodedPass(){
        return encodedPass;
    }

    public boolean check(String password){
        return SCryptUtil.check(password, encodedPass);
    }

}
