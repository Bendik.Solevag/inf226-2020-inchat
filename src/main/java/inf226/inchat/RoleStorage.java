package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;
import inf226.util.*;

public class RoleStorage {

    final Connection connection;
    final AccountStorage accountStorage;


    public RoleStorage(Connection connection, AccountStorage accountStorage) throws SQLException{
        this.connection = connection;
        this.accountStorage = accountStorage;
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Role (accountId TEXT, channelId TEXT, role TEXT, PRIMARY KEY(accountId, channelId))");
    }

    public void save(Stored<Account> account, Stored<Channel> channel, String role) throws SQLException {
        if(!(role.equals("owner") || role.equals("moderator") ||role.equals("participant") || role.equals("observer") || role.equals("banned"))){
            throw new SQLException();
        }
        PreparedStatement statement = connection.prepareStatement("INSERT INTO Role VALUES(?, ?, ?)");
        statement.setObject(1, account.identity);
        statement.setObject(2, channel.identity);
        statement.setString(3, role);
        statement.executeUpdate();
    }


    public void update(String userName, Stored<Channel> channel, String role) throws SQLException, DeletedException{
        Stored<Account> account = accountStorage.lookup(userName);
        PreparedStatement statement = connection.prepareStatement("SELECT role FROM Role WHERE accountId = ? AND channelId = ?");
        statement.setObject(1, account.identity);
        statement.setObject(2, channel.identity);
        ResultSet rs = statement.executeQuery();
        String existingRole = "";
        if(rs.next()){
            existingRole = rs.getString("role");
        }

        PreparedStatement numOwners = connection.prepareStatement("SELECT * FROM Role WHERE role='owner'");
        ResultSet owners = numOwners.executeQuery();
        int count = 0;
        while(owners.next()){
            count++;
        }

        if(count > 1 || !existingRole.equals("owner")){
            PreparedStatement updateStatement = connection.prepareStatement("UPDATE Role SET role = ? WHERE accountId = ? AND channelId = ?");
            updateStatement.setString(1, role);
            updateStatement.setObject(2, account.identity);
            updateStatement.setObject(3, channel.identity);
            updateStatement.executeUpdate();
        }
        else{
            System.err.println("Only one owner present, cannot change role");
        }
    }

    public String get(Stored<Account> account, Stored<Channel> channel) throws SQLException, DeletedException{
        PreparedStatement statement = connection.prepareStatement("SELECT Role.role FROM Role WHERE Role.accountId = ? AND Role.channelId = ?");
        statement.setObject(1, account.identity);
        statement.setObject(2, channel.identity);
        final ResultSet results = statement.executeQuery();
        if(results.next()){
            return results.getString("role");
        }
        throw new DeletedException();
    }

}
