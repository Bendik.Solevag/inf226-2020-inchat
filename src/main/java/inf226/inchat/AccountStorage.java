package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;

import inf226.util.immutable.List;
import inf226.util.*;

public final class AccountStorage
    implements Storage<Account,SQLException> {
    
    final Connection connection;
    final Storage<User,SQLException> userStore;
    final Storage<Channel,SQLException> channelStore;
    
    public AccountStorage(Connection connection,
                          Storage<User,SQLException> userStore,
                          Storage<Channel,SQLException> channelStore) 
      throws SQLException {
        this.connection = connection;
        this.userStore = userStore;
        this.channelStore = channelStore;

        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Account (id TEXT PRIMARY KEY, version TEXT, user TEXT, password TEXT, loginAttempts INT, FOREIGN KEY(user) REFERENCES User(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS AccountChannel (account TEXT, channel TEXT, alias TEXT, ordinal INTEGER, PRIMARY KEY(account,channel), FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Account> save(Account account)
      throws SQLException {


        final Stored<Account> stored = new Stored<Account>(account);
        try {
            PreparedStatement accountStatement = connection.prepareStatement("INSERT INTO Account VALUES(?, ?, ?, ?, ?)");
            accountStatement.setString(1, stored.identity.toString());
            accountStatement.setString(2, stored.version.toString());
            accountStatement.setString(3, account.user.identity.toString());
            accountStatement.setString(4, account.password.getEncodedPass());
            accountStatement.setInt(5, account.loginAttempts);
            accountStatement.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

        // Write the list of channels
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            try {
                PreparedStatement accountChannelStatement = connection.prepareStatement("INSERT INTO AccountChannel VALUES(?, ?, ?, ?)");
                accountChannelStatement.setString(1, stored.identity.toString());
                accountChannelStatement.setString(2, channel.identity.toString());
                accountChannelStatement.setString(3, alias);
                accountChannelStatement.setInt(4, ordinal.get());
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
        return stored;
    }
    
    @Override
    public synchronized Stored<Account> update(Stored<Account> account,
                                                Account new_account)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Account> current = get(account.identity);
    final Stored<Account> updated = current.newVersion(new_account);
    if(current.version.equals(account.version)) {
        PreparedStatement updateAccountStatement = connection.prepareStatement("UPDATE Account SET (version, user, loginAttempts) = (?, ?, ?) WHERE id = ?");
        updateAccountStatement.setString(1, updated.version.toString());
        updateAccountStatement.setString(2, new_account.user.identity.toString());
        updateAccountStatement.setInt(3, new_account.loginAttempts);
        updateAccountStatement.setString(4,  updated.identity.toString());
        updateAccountStatement.execute();

        // Rewrite the list of channels
        PreparedStatement deleteAccountStatement = connection.prepareStatement("DELETE FROM AccountChannel WHERE account = ?");
        deleteAccountStatement.setString(1, account.identity.toString());
        deleteAccountStatement.execute();
        
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        new_account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            try {
                PreparedStatement accountChannelStatement = connection.prepareStatement("INSERT INTO AccountChannel VALUES (?, ?, ?, ?)");
                accountChannelStatement.setString(1, account.identity.toString());
                accountChannelStatement.setString(2, channel.identity.toString());
                accountChannelStatement.setString(3, alias);
                accountChannelStatement.setInt(4, ordinal.get());
                accountChannelStatement.execute();
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
    } else {
        throw new UpdatedException(current);
    }
    return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Account> account)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Account> current = get(account.identity);
        if(current.version.equals(account.version)) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Account WHERE id = ?");
            statement.setString(1, account.identity.toString());
            statement.execute();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Account> get(UUID id)
      throws DeletedException,
             SQLException {

        PreparedStatement accStatement = connection.prepareStatement("SELECT version, user, password, loginAttempts FROM Account WHERE id = ?");
        accStatement.setString(1,id.toString());

        PreparedStatement chaStatement = connection.prepareStatement("SELECT channel, alias, ordinal FROM AccountChannel WHERE account = ? ORDER BY ordinal DESC");
        chaStatement.setString(1, id.toString());

        final ResultSet accountResult = accStatement.executeQuery();
        final ResultSet channelResult = chaStatement.executeQuery();


        if(accountResult.next()) {
            final UUID version = UUID.fromString(accountResult.getString("version"));
            final UUID userid = UUID.fromString(accountResult.getString("user"));
            final String pass = accountResult.getString("password");
            final int attempts = accountResult.getInt("loginAttempts");
            final Stored<User> user = userStore.get(userid);

            // Get all the channels associated with this account
            final List.Builder<Pair<String,Stored<Channel>>> channels = List.builder();

            while(channelResult.next()) {
                final UUID channelId =
                    UUID.fromString(channelResult.getString("channel"));
                final String alias = channelResult.getString("alias");
                channels.accept(
                    new Pair<String,Stored<Channel>>(
                        alias,channelStore.get(channelId)));
            }
            return (new Stored<Account>(new Account(user,channels.getList(), new Password(pass), attempts),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    public Stored<Account> lookup(String username)
      throws DeletedException,
             SQLException {
        PreparedStatement statement1 = connection.prepareStatement("SELECT Account.id from Account INNER JOIN User ON user=User.id WHERE User.name = ?");
        statement1.setString(1, username);

        final ResultSet rs = statement1.executeQuery();
        if(rs.next()) {
            final UUID identity = 
                    UUID.fromString(rs.getString("id"));

            return get(identity);
        }
        throw new DeletedException();
    }

} 
 
