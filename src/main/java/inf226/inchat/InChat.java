package inf226.inchat;

import inf226.storage.*;
import inf226.util.Maybe;
import inf226.util.Util;

import java.util.TreeMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.UUID;
import java.time.Instant;
import java.sql.SQLException;


import inf226.util.immutable.List;

/**
 * This class models the chat logic.
 *
 * It provides an abstract interface to
 * usual chat server actions.
 *
 **/

public class InChat {
    private final UserStorage userStore;
    private final ChannelStorage channelStore;
    private final AccountStorage accountStore;
    private final SessionStorage sessionStore;
    private final RoleStorage roleStore;
    private final Map<UUID,List<Consumer<Channel.Event>>> eventCallbacks
        = new TreeMap<UUID,List<Consumer<Channel.Event>>>();

    public InChat(UserStorage userStore,
                  ChannelStorage channelStore,
                  AccountStorage accountStore,
                  SessionStorage sessionStore,
                  RoleStorage roleStore) {
        this.userStore=userStore;
        this.channelStore=channelStore;
        this.accountStore=accountStore;
        this.sessionStore=sessionStore;
        this.roleStore = roleStore;
    }


    /**
     * Log in a user to the chat.
     */
    public Maybe<Stored<Session>> login(String username, String password) {
        // Here you can implement login.
        try {
            final Stored<Account> account = accountStore.lookup(username);
            //Check password, limit login attempts to 10.
            if(account.value.checkPassword(password) && account.value.loginAttempts <= 10){
                final Stored<Session> session =
                sessionStore.save(new Session(account, Instant.now().plusSeconds(60*60*24)));
                accountStore.update(account, new Account(account.value.user, account.value.channels, account.value.password, 0));
                return Maybe.just(session);
            }
            else{
                accountStore.update(account, new Account(account.value.user, account.value.channels, account.value.password, account.value.loginAttempts + 1));
            }
        } catch (SQLException e) {
        } catch (DeletedException e) {
        } catch (UpdatedException e) {
        }
        return Maybe.nothing();
    }
    
    /**
     * Register a new user.
     */
    public Maybe<Stored<Session>> register(String username, String password) {

        try{
            //If call succeeds, user by this username already exists. Return.
            final Stored<Account> checkIfUserExists = accountStore.lookup(username);
            return Maybe.nothing();
        }
        catch(Exception er){
            //If Exception is thrown, no user by username exists. Create account.
            try {
                final Stored<User> user =
                        userStore.save(User.create(username));
                final Stored<Account> account =
                        accountStore.save(Account.create(user, password));
                final Stored<Session> session =
                        sessionStore.save(new Session(account, Instant.now().plusSeconds(60*60*24)));
                return Maybe.just(session);
            } catch (SQLException e) {
                e.printStackTrace();
                return Maybe.nothing();
            }
            catch(IllegalArgumentException e){
                e.printStackTrace();
                return Maybe.nothing();
            }
        }
    }
    
    /**
     * Restore a previous session.
     */
    public Maybe<Stored<Session>> restoreSession(UUID sessionId) {
        try {
            return Maybe.just(sessionStore.get(sessionId));
        } catch (SQLException e) {
            System.err.println("When restoring session:" + e);
            return Maybe.nothing();
        } catch (DeletedException e) {
            return Maybe.nothing();
        }
    }
    
    /**
     * Log out and invalidate the session.
     */
    public void logout(Stored<Session> session) {
        try {
            Util.deleteSingle(session,sessionStore);
        } catch (SQLException e) {
            System.err.println("When loging out of session:" + e);
        }
    }
    
    /**
     * Create a new channel.
     */
    public Maybe<Stored<Channel>> createChannel(Stored<Account> account,
                                                String name) {
        try {
            Stored<Channel> channel
                = channelStore.save(new Channel(name,List.empty()));
            return joinChannel(account, channel.identity, "owner");
        } catch (SQLException e) {
            System.err.println("When trying to create channel " + name +":\n" + e);
        }
        return Maybe.nothing();
    }
    
    /**
     * Join a channel.
     */
    public Maybe<Stored<Channel>> joinChannel(Stored<Account> account,
                                              UUID channelID,
                                              String role) {
        try {
            Stored<Channel> channel = channelStore.get(channelID);
            roleStore.save(account, channel, role);
            Util.updateSingle(account,
                    accountStore,
                    a -> a.value.joinChannel(channel.value.name,channel));
            Stored<Channel.Event> joinEvent
                    = channelStore.eventStore.save(
                    Channel.Event.createJoinEvent(Instant.now(),
                            account.value.user.value.name.getUserName()));
            return Maybe.just(
                    Util.updateSingle(channel,
                            channelStore,
                            c -> c.value.postEvent(joinEvent)));
        } catch (DeletedException e) {
            // This channel has been deleted.
        } catch (SQLException e) {
            System.err.println("When trying to join " + channelID +":\n" + e);
        }
        return Maybe.nothing();
    }

    
    /**
     * Post a message to a channel.
     */
    public Maybe<Stored<Channel>> postMessage(Stored<Account> account,
                                              Stored<Channel> channel,
                                              String message) {
        try {
            Stored<Channel.Event> event
                    = channelStore.eventStore.save(
                    Channel.Event.createMessageEvent(Instant.now(),
                            account.value.user.value.name.getUserName(), message));
            if(canPostMessage(account, channel)){
                try {
                    return Maybe.just(
                            Util.updateSingle(channel,
                                    channelStore,
                                    c -> c.value.postEvent(event)));
                } catch (DeletedException e) {
                    // Channel was already deleted.
                    // Let us pretend this never happened
                    Util.deleteSingle(event, channelStore.eventStore);
                }
            }
            else{
                return Maybe.just(channel);
            }
        } catch (SQLException e) {
            System.err.println("When trying to post message in " + channel.identity +":\n" + e);
        }
        return Maybe.nothing();
    }

    
    /**
     * A blocking call which returns the next state of the channel.
     */
    public Maybe<Stored<Channel>> waitNextChannelVersion(UUID identity, UUID version) {
        try {
            return Maybe.just(channelStore.waitNextVersion(identity, version));
        } catch (SQLException e) {
            System.err.println("While waiting for the next message in " + identity +":\n" + e);
        } catch (DeletedException e) {
            // Channel deleted.
        }
        return Maybe.nothing();
    }
    
    public Maybe<Stored<Channel.Event>> getEvent(UUID eventID) {
        try {
            return Maybe.just(channelStore.eventStore.get(eventID));
        } catch (SQLException e) {
            return Maybe.nothing();
        } catch (DeletedException e) {
            return Maybe.nothing();
        }
    }

    public Stored<Channel> deleteEvent(Stored<Account> account, Stored<Channel> channel, Stored<Channel.Event> event) {
        if(canEditMessage(account, channel, event)){
            try {
                Util.deleteSingle(event , channelStore.eventStore);
                return channelStore.noChangeUpdate(channel.identity);
            } catch (SQLException er) {
                System.err.println("While deleting event " + event.identity +":\n" + er);
            } catch (DeletedException er) {
                er.printStackTrace();
            }
        }
        return channel;
    }


    public Stored<Channel> editMessage(Stored<Account> account,
                                       Stored<Channel> channel,
                                       Stored<Channel.Event> event,
                                       String newMessage) {
        if(canEditMessage(account, channel, event)){
            try{
                Util.updateSingle(event,
                        channelStore.eventStore,
                        e -> e.value.setMessage(newMessage));
                return channelStore.noChangeUpdate(channel.identity);
            } catch (SQLException er) {
                System.err.println("While deleting event " + event.identity +":\n" + er);
            } catch (DeletedException er) {
                er.printStackTrace();
            }
        }
        return channel;
    }


    public void setRole(Stored<Account> caller, Stored<Channel> channel, String userName, String role){
        try{

            if(canSetRole(caller, channel)){
                roleStore.update(userName, channel, role);
            }
        }
        catch(Exception e){
            System.err.println("Error while setting role: " + e);
            e.printStackTrace();
        }
    }

    private boolean canSetRole(Stored<Account> account, Stored<Channel> channel){
        try{
            return roleStore.get(account, channel).equals("owner");
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    private boolean canEditMessage(Stored<Account> account, Stored<Channel> channel, Stored<Channel.Event> message){

        try {
            boolean editor = roleStore.get(account, channel).equals("moderator") || roleStore.get(account, channel).equals("owner");
            boolean author = channelStore.eventStore.get(message.identity).value.sender.equals(account.identity.toString());
            return editor || author;
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    private boolean canPostMessage(Stored<Account> account, Stored<Channel> channel){
        try{
            return !(roleStore.get(account, channel).equals("banned") || roleStore.get(account, channel).equals("observer"));
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean canViewMessage(Stored<Account> account, Stored<Channel> channel){
        try {
            return (!roleStore.get(account, channel).equals("banned"));
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}


