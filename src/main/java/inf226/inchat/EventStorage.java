package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, type INTEGER, time TEXT)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event)
      throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);
        PreparedStatement statement = connection.prepareStatement("INSERT INTO Event VALUES(?, ?, ?, ?)");
        statement.setString(1, stored.identity.toString());
        statement.setString(2, stored.version.toString());
        statement.setInt(3, event.type.code);
        statement.setString(4, event.time.toString());
        statement.execute();


        switch (event.type) {
            case message:
                PreparedStatement message = connection.prepareStatement("INSERT INTO Message VALUES(?, ?, ?)");
                message.setString(1, stored.identity.toString());
                message.setString(2, event.sender);
                message.setString(3, event.message);
                message.execute();
                break;
            case join:
                PreparedStatement join = connection.prepareStatement("INSERT INTO Joined VALUES (?, ?)");
                join.setString(1, stored.identity.toString());
                join.setString(2, event.sender);
                join.execute();
                break;
        }
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event,
                                            Channel.Event new_event)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        PreparedStatement updateStatement = connection.prepareStatement("UPDATE Event SET (version, time, type) = (?, ?, ?) WHERE id = ?");
        updateStatement.setString(1, updated.version.toString());
        updateStatement.setString(2, new_event.time.toString());
        updateStatement.setInt(3, new_event.type.code);
        updateStatement.setString(4, updated.identity.toString());
        updateStatement.execute();

        switch (new_event.type) {
            case message:
                PreparedStatement message = connection.prepareStatement("UPDATE Message SET (sender, content) = (?, ?) WHERE id = ?");
                message.setString(1, new_event.sender);
                message.setString(2, new_event.message);
                message.setString(3, updated.identity.toString());
                message.execute();
                break;
            case join:
                PreparedStatement join = connection.prepareStatement("UPDATE Joined SET (sender) = (?) WHERE id = ?");
                join.setString(1, new_event.sender);
                join.setString(2, updated.identity.toString());
                break;
        }
    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Event WHERE id = ?");
            statement.setString(1, event.identity.toString());
            statement.execute();
        } else {
            throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id)
      throws DeletedException,
             SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT version, time, type FROM Event WHERE id = ?");
        statement.setString(1, id.toString());
        final ResultSet rs = statement.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));
            
            final Statement mstatement = connection.createStatement();
            switch(type) {
                case message:
                    PreparedStatement message = connection.prepareStatement("SELECT sender, content FROM Message WHERE id = ?");
                    message.setString(1, id.toString());
                    final ResultSet mrs = message.executeQuery();
                    mrs.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(time,mrs.getString("sender"),mrs.getString("content")),
                            id,
                            version);
                case join:
                    PreparedStatement join = connection.prepareStatement("SELECT sender FROM Joined WHERE id = ?");
                    join.setString(1, id.toString());
                    final ResultSet ars = join.executeQuery();
                    ars.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(time,ars.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 
