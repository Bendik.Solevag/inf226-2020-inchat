package inf226.inchat;
import com.lambdaworks.crypto.SCrypt;
import com.lambdaworks.crypto.SCryptUtil;
import inf226.util.immutable.List;
import inf226.util.Pair;


import inf226.storage.*;

/**
 * The Account class holds all information private to
 * a specific user.
 **/
public final class Account {
    /*
     * A channel consists of a User object of public account info,
     * and a list of channels which the user can post to.
     * user.
     */
    public final Stored<User> user;
    public final List<Pair<String,Stored<Channel>>> channels;
    public final Password password;
    public final int loginAttempts;

    public Account(Stored<User> user,
                   List<Pair<String,Stored<Channel>>> channels,
                   Password encodedPass,
                   int loginAttempts) {
        this.user = user;
        this.channels = channels;
        this.password = encodedPass;
        this.loginAttempts = loginAttempts;
    }

    /**
     * Create a new Account.
     *
     * @param user The public User profile for this user.
     * @param password The login password for this account.
     **/
    public static Account create(Stored<User> user,
                                 String password) {
        //NIST reccomends at least 8 character passwords
        if(password.length() < 8){
            throw new IllegalArgumentException();
        }

        password = SCryptUtil.scrypt(password, 16384, 8, 1);
        Password encodedPassword = new Password(password);
        return new Account(user,List.empty(), encodedPassword, 0);
    }

    /**
     * Join a channel with this account.
     *
     * @return A new account object with the cannnel added.
     */
    public Account joinChannel(String alias, Stored<Channel> channel) {
        Pair<String,Stored<Channel>> entry
                = new Pair<String,Stored<Channel>>(alias,channel);
        return new Account
                (user,
                List.cons(entry,
                        channels),
                password,
                loginAttempts);
    }


    /**
     * Check weather if a string is a correct password for
     * this account.
     *
     * @return true if password matches.
     */
    public boolean checkPassword(String password) {
        return this.password.check(password);
    }


}